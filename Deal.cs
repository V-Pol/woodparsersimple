﻿using System;

namespace WoodParser
{
    internal class Deal
    {
        public string DealId { get; set; }
        public string SellerName { get; set; }
        public string SellerINN { get; set; }
        public string BuyerName { get; set; }
        public string BuyerINN { get; set; }
        public string DealDate { get; set; }
        public string VolumeSeller { get; set; }
        public string VolumeBuyer { get; set; }
    }
}
