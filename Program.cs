﻿using OpenQA.Selenium;
using OpenQA.Selenium.Edge;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace WoodParser
{
    internal class Program
    {
        private static Timer timer;
        private static IWebDriver driver;
        private static List<Deal> deals = new List<Deal>();
        private static SqlCommand command = new SqlCommand();
        private static int counterCheckEmpty;
        static void Main(string[] args)
        {
            timer = new Timer(ParseHtml, null, 0, Timeout.Infinite);
            Console.ReadLine();
        }

        private static void ParseHtml(object obj)
        {
            timer.Change(Timeout.Infinite, Timeout.Infinite);
            string connectionString = "Server=(local)\\SQLEXPRESS;Database=wood;User Id=sa;Password=P@ssw0rd;";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                OpenUrl();
                connection.Open();
                if (connection.State.ToString() == "Open")
                {
                    command.Connection = connection;
                    Console.WriteLine("Подключение к БД открыто");

                    var sortDateButton = driver.FindElement(By.CssSelector("div.ag-header-cell[col-id='dealDate']>div.ag-cell-label-container"));
                    sortDateButton.Click();
                    sortDateButton.Click();

                    Console.WriteLine($"Обработана страница ");
                    MatchCollection matches;
                    do
                    {
                        var pages = driver.FindElement(By.ClassName("x-toolbar-text")).Text;
                        Regex regex = new Regex(@"\d+\s*\,*\d*", RegexOptions.IgnoreCase);
                        matches = regex.Matches(pages);

                        GetDataFromPage();
                        CheckDealsInDB();

                        if (counterCheckEmpty > 2) //проверка на уже распарсерные страницы
                        {
                            Console.WriteLine("Новые данные для загрузки отсутствуют");
                            break;
                        }

                        InsertIntoDB();
                        Console.WriteLine($"{matches[0]} из {matches[1]}");

                        driver.FindElement(By.ClassName("x-tbar-page-next")).Click();
                        Thread.Sleep(1000);
                    }
                    while (matches[0] != matches[1]);
                    counterCheckEmpty = 0;
                }
                else
                {
                    Console.WriteLine("Не удалось подключиться к БД");
                }
            }
            Console.WriteLine("Подключение  к БД закрыто");
            timer.Change(1000*60*10, 0);
        }

        private static void OpenUrl()
        {
            var path = Environment.CurrentDirectory;
            var edgeOptions = new EdgeOptions();
            edgeOptions.AddArgument("headless");
            edgeOptions.AddArgument("--silent");
            driver = new EdgeDriver(path, edgeOptions);
            driver.Url = @"https://www.lesegais.ru/open-area/deal";
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(5);
            Console.WriteLine("Подключение к сайту открыто");
            Thread.Sleep(3000);
        }

        private static void GetDataFromPage()
        {
            var rows = GetElementsFromPage(".ag-row");
            foreach (var row in rows)
            {
                var rowIndex = row.GetAttribute("row-index");
                if (rowIndex != "20" && rowIndex != "21")
                {
                    var cells = GetElementsFromPage($"div.ag-row[row-index='{rowIndex}']>div.ag-cell");
                    AddToDealLists(cells);
                }
            }
        }

        private static ReadOnlyCollection<IWebElement> GetElementsFromPage(string cssSelector)
        {
            return driver.FindElements(By.CssSelector(cssSelector));
        }

        private static void AddToDealLists(ReadOnlyCollection<IWebElement> webElements)
        {
            var values = webElements.Select(el => el.Text).ToList();
            if (deals.FindIndex(s => s.DealId == values[0]) == -1)
            {
                Regex regex = new Regex(@"\d+\s*\,*\d*", RegexOptions.IgnoreCase);
                var matches = regex.Matches(values[6]);
                if (matches.Count > 0)
                {
                    var volumeSeller = matches[0].Value.Replace(" ", "");
                    var volumeByer = matches[1].Value.Replace(" ", "");
                    deals.Add(new Deal
                    {
                        DealId = values[0],
                        SellerName = values[1],
                        SellerINN = values[2],
                        BuyerName = values[3],
                        BuyerINN = values[4],
                        DealDate = values[5],
                        VolumeSeller = volumeSeller,
                        VolumeBuyer = volumeByer,
                    });
                }
            }
        }

        private static void CheckDealsInDB()
        {
            if (deals.Count > 0)
            {
                StringBuilder sqlExpression = new StringBuilder("SELECT DealId FROM Deals WHERE DealId IN (");
                foreach (var deal in deals)
                {
                    sqlExpression.Append($"'{deal.DealId}', ");
                }
                sqlExpression.Replace(',',')',sqlExpression.Length - 2, 1);
                command = new SqlCommand(sqlExpression.ToString(), command.Connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var id = reader.GetValue(0).ToString();
                        var item = deals.FirstOrDefault(s => s.DealId == id);
                        deals.Remove(item);
                        if (deals.Count == 0)
                            counterCheckEmpty++;
                    }
                }
                reader.Close();
            }
        }

        private static void InsertIntoDB()
        {
            if (deals.Count > 0)
            {
                deals.ForEach(d => d.VolumeSeller = d.VolumeSeller.Replace(',', '.'));
                deals.ForEach(d => d.VolumeBuyer = d.VolumeBuyer.Replace(',', '.'));
                StringBuilder sqlExpression = new StringBuilder(
                    "INSERT INTO Deals ([DealId], [SellerName], [SellerINN], [BuyerName], [BuyerINN], [DealDate], [VolumeSeller], [VolumeBuyer]) VALUES ");
                foreach (var item in deals)
                {
                    sqlExpression.Append(
                        $"('{item.DealId}', N'{item.SellerName}', '{item.SellerINN}', N'{item.BuyerName}', N'{item.BuyerINN}', CONVERT(DATETIME,'{item.DealDate}', 104), {item.VolumeSeller}, {item.VolumeBuyer}),");
                }
                sqlExpression.Remove(sqlExpression.Length - 1, 1);
                command = new SqlCommand(sqlExpression.ToString(), command.Connection);
                command.ExecuteNonQuery();
            }
        }
    }
}